1) For the app to register beacons in the background, you'll need to create a MainApplication class extending application:

public class MainApplication  extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        PinchManager.getInstance().setDebug(false);
        PinchManager.getInstance().setUrlScheme(<your-unique-hostname><your-unique-urlscheme>); // Eg "myuniqueapp://myunique.hostname.com"
        PinchManager.getInstance().setNotificationResId(R.drawable.icon);
        PinchManager.getInstance().init(this);
        PinchManager.getInstance().start();
    }

}

2) Then add android:name="MainApplication" to the application tag in AndroidManifest.xml

3) Make sure you've got the correct AppID and AppSecret in res/values/pinch.xml

4) Update the TermsUrl in res/values/pinch.xml

5) Finally add this below the existing <intent-filter> under application -> activity in AndroidManifest.xml:

 <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <category android:name="android.intent.category.BROWSABLE" />
                <data android:host="<your-unique-hostname>" android:scheme="<your-unique-urlscheme>" />  "myunique.hostname.com" and "myuniqueapp"
 </intent-filter>


PS: If possible, build the app with target lower than SDK23 to avoid the new permission requirements in Android 6.0