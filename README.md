# Pinch SDK Cordova Pinch Proximity Plugin implementation guide 

[![N|Solid](http://ganymede.blob.core.windows.net/upload/fluxloop-logo-wide-12001_small.jpg)](https://nodesource.com/products/nsolid)

PinchSDK is available for Android 4.3+ (API level 18) and iOS 8.0 (iPhone 4S or later). For Android, devices running on API level prior to API level 18, will have Pinch disabled. The minimum level for building PinchSDK is 14.

Native implementation guide and downloads are available on our wiki pages: https://bitbucket.org/fluxloop/pinch.installpackage/wiki/Home


## Installation
#### Adding plugin with Config.Xml

```xml

<plugin name="cordova-plugin-pinch">
	<!-- Required variables for pinch plugin -->
    <variable name="PINCH_APPID" value="CONTACT FLUXLOOP FOR APP ID" />
    <variable name="PINCH_APPSECRET" value="CONTACT FLUXLOOP FOR APP TOKEN" />
    <variable name="APP_LOCATIONDESCRIPTION" value="ADD DESCRIPTION FOR LOCATION ALWAYS AUTHORIZATION" />
    <variable name="APP_URLNAME" value="APP IDENTIFIER FOR URL SCHEME" />
    <variable name="APP_URLSCHEME" value="APP URL SCHEME" />        
	<!-- Following variables are optional -->
    <variable name="PINCH_BUBBLEFOREGROUNDCOLOR" value="#ffffff" />
	<variable name="PINCH_BUBBLEBACKGROUNDCOLOR" value="#000000" />
	<variable name="PINCH_BUBBLEBORDERCOLOR" value="#ffffff" />
	<variable name="PINCH_SETTINGS_TITLE" value="Location based communication" />
	<variable name="PINCH_ENABLEPINCH_TITLE" value="Personalised content" />
	<variable name="PINCH_DELETEDATA_TITLE" value="Delete my data" /> 
</plugin>
```

#### Adding plugin with CLI
```sh
// npm hosted id
cordova plugin add cordova-plugin-pinch --variable PINCH_APPID=<APPID-FROM-FLUXLOOP> --variable PINCH_APPSECRET=<APPTOKEN-FROM-FLUXLOOP> --variable APP_LOCATIONDESCRIPTION="To detect ibeacons when the application is not running" --variable APP_URLNAME=<com.company.myapp> --variable APP_URLSCHEME=<myapp>

// you may also install directly from this repo
cordova plugin add https://bitbucket.org/fluxloop/cordova-plugin-pinch.git --variable PINCH_APPID=<APPID-FROM-FLUXLOOP> --variable PINCH_APPSECRET=<APPTOKEN-FROM-FLUXLOOP> --variable APP_LOCATIONDESCRIPTION="To detect ibeacons when the application is not running" --variable APP_URLNAME=<com.company.myapp> --variable APP_URLSCHEME=<myapp>
```

#### Dependencies

After the plugin is installed it tries to execute an install script which performs various configuration.
The script has some depended npm modules which should be installed automatically. If this for some reason does not happen, the problems should be resolved by installing them manually from the cordova project root path.

```sh
npm install shelljs
```


## Using Pinch

This plugin defines a global `Pinch` object.
Although in the global scope, it is not available until after the deviceready event.

### Initialize pinch

```javascript
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
	// Start pinch and init views used by pinch in the background
    pinch.initViews();
}        
```


### Other methods

#### Request permissions
```javascript

// Request permission to  show notifications
pinch.requestNotificationAuthorization();   

// Request permission to location in the background, even though the application is not running.
pinch.requestLocationAuthorization();

// Trigger default native authorization process in pinch. More information coming soon.
pinch.checkAuthorization();

```

#### Check permissions and status
```javascript

// Check if app has permission to display notifications
pinch.hasNotificationPermission(function(success){ 
	console.log(success); 
});

// Check if app has permission to location in the background
pinch.hasLocationPermission(function(success){ 
	console.log(success); 
});

// Check if the OS has requested location permission
pinch.getLocationRequested(function(wasrequsted){ 
	console.log(wasrequsted); 
});

// Check if the OS has requested notification permission
pinch.getNotificationRequested(function(wasrequsted){ 
	console.log(wasrequsted); 
});

// Get bluetooth status. More information coming soon.    
pinch.checkBluetoothStatus(function(bluetoothStatus){
	console.log(bluetoothStatus); 
});   

// Get notification settings. More information coming soon.
pinch.checkNotificationSettings(function(notificationSettings){
	console.log(notificationSettings); 
});   
   
// Get location settings. More information coming soon.
pinch.checkLocationSettings(function(locationSettings){
	console.log(locationSettings); 
});
```

#### Register data to pinch
```javascript 
// Send username to pinch
var username = "exampleUserName";
pinch.registerUserName(username);

// Send any userdata to pinch
var modelname = "UserData"; // Example identifier for data
var userdata = { firstName:"John", lastName:"Doe", email:"john.doe@gmail.com"}; // Example data
var jsonUserdata = JSON.stringify(jsonUserdata); // Data needs to be a string
pinch.registerUserData(modelname,jsonUserdata);

// Send an event to pinch
var eventName = "LoggedIn"; // Example event
pinch.registerEvent(eventName);
```   

#### Save and get key value data natively in the application
```javascript 
// Get a saved value
pinch.getUserDefaults("key", function(value) {
	console.log(value);
}); 

// Save a value
pinch.saveUserDefaults("key", "value");

// Remove a saved value
pinch.removeUserDefaults("key");
```  

#### Handle campaigns in Pinch
```javascript 
// Get the last recieved campaign data
pinch.getLatestCampaign(function(camapignData) {
	console.log(camapignData);
});

// Simulate a recieved campaign. The view url is hardcoded natively for now. 
pinch.simulateCampaign();

// Close the current campaign view.
pinch.closeCampaign();

// Delete the current campaign. The current wiew will also be closed.
pinch.deleteCampaign();

// Open the last recieved campaign.
pinch.openCampaign();

// Simulate a recieved campaign with a custom URL and viewtype.
var campaignUrl = "https://www.yourdomain.com/youcampaign";
// 1 = popover, 2 = fullscreeen, 3 = browser
var viewtype = 1; 
pinch.openCampaignWithUrl(url,viewtype);

```  

#### Get user identifiers
```javascript 
// Get user identifier used by Pinch
pinch.getPinchID(function(pinchId){
	console.log(pinchId);
});        

// Get Advertising identifier, if available.
pinch.getAdvertisingIdentifier(function(adid){
	console.log(adid);
});        

``` 
     

#### Enable or disable Pinch
```javascript 
// Send true or false to enable or disable Pinch. By default pinch will be enabled.
pinch.setPinchEnabled(enabled);        
``` 
          
#### Events
```javascript 
// Get events from pinch. More information coming soon.
pinch.addEventListener(eventSelector, callback); 

// Example, subscribe to all pinch events.
pinch.addEventListener("pinch", function (eventName, eventData) {
    console.log(eventName);
});
```