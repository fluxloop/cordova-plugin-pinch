
var DefaultApplicationName = "com.fluxloop.cordova.MainApplication";

module.exports = function(context) {

    try {
        var path = context.requireCordovaModule('path');
        var ConfigFile = context.requireCordovaModule("cordova-common").ConfigFile;

        if (context.opts.cordova.platforms.indexOf("android") > -1) {          

            var androidPrjDir = path.join(context.opts.projectRoot, 'platforms/android');
            var androidManifest = new ConfigFile(androidPrjDir, 'android', 'AndroidManifest.xml');

            var applicationNode = androidManifest.data.find('application');            
            if (applicationNode.get('android:name') == DefaultApplicationName) {
                console.log("PINCH: Removing android:name added by pinch install.");
                delete applicationNode.attrib['android:name'];            
            }
            
            var activityNode = androidManifest.data.find(".//activity");
            if (activityNode) {
                var intentNode = androidManifest.data.find('.//intent-filter[@android:name="pinch"]');
                if (intentNode) {
                  console.log("PINCH: Removing Pinch intent filter from AndroidManifest.xml");
                  activityNode.remove(intentNode);
                }
            }    
            
            androidManifest.save();        
            console.log("PINCH: Uninstall script completed successfully.");
        }
    }
    catch(ex) {
      console.log('\x1b[31m\x1b[1m%s\x1b[0m', "PINCH: Uninstall script failed. Some settings could not be removed.");        
    }

    return true;
}
