
var DefaultApplicationName = "com.fluxloop.cordova.MainApplication";
var MultiDexApplicationName = "com.fluxloop.cordova.PinchMultiDexApplication";

module.exports = function(context) {

    console.log("PINCH: Starting plugin configuration script");


    try {
        require('shelljs/global');
        var fs = require('fs');

        var path = context.requireCordovaModule('path');
        var ConfigFile = context.requireCordovaModule("cordova-common").ConfigFile;
        var configXml = new ConfigFile(context.opts.projectRoot, null, './config.xml');

        // Get the plugin element
        var pinchPluginNode = configXml.data.find('plugin[@name="cordova-plugin-pinch"]');
        if (pinchPluginNode==null) pinchPluginNode = {find: function() { return null; }}

        var BUNDLEID = configXml.data._root.attrib.id;
        var APPNAME = configXml.data.find("name").text;

        var App_UrlScheme = "";
        var App_UrlName = "";
        var App_LocationDescription = "";
        var App_LocationWhenInUseDescription = "";
        var App_LocationAlwaysDescription = "";
        var Pinch_AppId = "";
        var Pinch_AppSecret = "";

        var Pinch_BubbleForegroundColor = "";
        var Pinch_BubbleBackgroundColor = "";
        var Pinch_BubbleBorderColor = "";
        var Pinch_EnablePinch_Title = "";
        var Pinch_DeleteData_Title = "";
        var Pinch_Setttings_Title= "";
        var Pinch_Authorization_PrimaryColor = "";
        var Pinch_Authorization_SecondaryColor = "";
        var Pinch_Authorization_LocationImage = "";
        var Pinch_Authorization_NotificationImage = "";
        var Pinch_AuthorizeOnStartup = "false";
        var Pinch_UseAuthorizationUI = "false";
        var Pinch_HandleAuthorization = "false";

        // Get variables passed from command line
        if(context.hasOwnProperty("cmdLine")){
          var aVariables = context.cmdLine.split("--variable");
          for(c in aVariables){
            var variable = aVariables[c].trim().split('=');
            if (variable[0]=="PINCH_APPID") Pinch_AppId = variable[1].split(' ')[0];
            if (variable[0]=="PINCH_APPSECRET") Pinch_AppSecret = variable[1].split(' ')[0];
            if (variable[0]=="APP_URLSCHEME") App_UrlScheme = variable[1].split(' ')[0];
            if (variable[0]=="APP_URLNAME") App_UrlName = variable[1].split(' ')[0];
            if (variable[0]=="PINCH_AUTHORIZEONSTARTUP") Pinch_AuthorizeOnStartup = variable[1].split(' ')[0];
            if (variable[0]=="PINCH_USEAUTHORIZATIONUI") Pinch_UseAuthorizationUI = variable[1].split(' ')[0];
            if (variable[0]=="PINCH_HANDLEAUTHORIZATION") Pinch_HandleAuthorization = variable[1].split(' ')[0];
          }
        }        

        // Exit if Pinch_AppID is missing
        if (Pinch_AppId=="" && !pinchPluginNode.find('variable[@name="PINCH_APPID"]')) {
            throw ("PINCH missing variable 'PINCH_APPID' in Config.Xml");
        }

        // Exit if Pinch_AppSecret is missing
        if (Pinch_AppSecret=="" && !pinchPluginNode.find('variable[@name="PINCH_APPSECRET"]')) {
            throw ("PINCH missing variable 'PINCH_APPSECRET' in Config.Xml");
        }

        // Exit if App_UrlScheme is missing
        if (App_UrlScheme=="" && !pinchPluginNode.find('variable[@name="APP_URLSCHEME"]')) {
            throw ("PINCH missing variable 'APP_URLSCHEME' in Config.Xml");
        }

        // Exit if App_UrlName is missing
        if (App_UrlName=="" && !pinchPluginNode.find('variable[@name="APP_URLNAME"]')) {
            throw ("PINCH missing variable 'APP_URLNAME' in Config.Xml");
        }

        // Use settings from config.xml if found
        var Pinch_AppId_Element = pinchPluginNode.find('variable[@name="PINCH_APPID"]');
        if (Pinch_AppId_Element) Pinch_AppId = Pinch_AppId_Element.get("value");

        var App_UrlScheme_Element = pinchPluginNode.find('variable[@name="APP_URLSCHEME"]');
        if (App_UrlScheme_Element) App_UrlScheme = App_UrlScheme_Element.get("value");

        var App_UrlName_Element = pinchPluginNode.find('variable[@name="APP_URLNAME"]');
        if (App_UrlName_Element) App_UrlName = App_UrlName_Element.get("value");

        var App_LocationWhenInUseDescription_Element = pinchPluginNode.find('variable[@name="APP_LOCATIONWHENINUSEDESCRIPTION"]');
        if (App_LocationWhenInUseDescription_Element) App_LocationWhenInUseDescription = App_LocationWhenInUseDescription_Element.get("value");

        var App_LocationAlwaysDescription_Element = pinchPluginNode.find('variable[@name="APP_LOCATIONALWAYSDESCRIPTION"]');
        if (App_LocationAlwaysDescription_Element) App_LocationAlwaysDescription = App_LocationAlwaysDescription_Element.get("value");

        var App_LocationDescription_Element = pinchPluginNode.find('variable[@name="APP_LOCATIONDESCRIPTION"]');
        if (App_LocationDescription_Element) {
          App_LocationDescription = App_LocationDescription_Element.get("value");
          if (!App_LocationWhenInUseDescription_Element) App_LocationWhenInUseDescription = App_LocationDescription;
          if (!App_LocationAlwaysDescription_Element) App_LocationAlwaysDescription = App_LocationDescription;
        }

        var Pinch_AppSecret_Element = pinchPluginNode.find('variable[@name="PINCH_APPSECRET"]');
        if (Pinch_AppSecret_Element) Pinch_AppSecret = Pinch_AppSecret_Element.get("value");

        var Pinch_BubbleForegroundColor_Element = pinchPluginNode.find('variable[@name="PINCH_BUBBLEFOREGROUNDCOLOR"]');
        if (Pinch_BubbleForegroundColor_Element) Pinch_BubbleForegroundColor = Pinch_BubbleForegroundColor_Element.get("value");

        var Pinch_BubbleBackgroundColor_Element = pinchPluginNode.find('variable[@name="PINCH_BUBBLEBACKGROUNDCOLOR"]');
        if (Pinch_BubbleBackgroundColor_Element) Pinch_BubbleBackgroundColor = Pinch_BubbleBackgroundColor_Element.get("value");

        var Pinch_BubbleBorderColor_Element = pinchPluginNode.find('variable[@name="PINCH_BUBBLEBORDERCOLOR"]');
        if (Pinch_BubbleBorderColor_Element) Pinch_BubbleBorderColor = Pinch_BubbleBorderColor_Element.get("value");

        var Pinch_EnablePinch_Title_Element = pinchPluginNode.find('variable[@name="PINCH_ENABLEPINCH_TITLE"]');
        if (Pinch_EnablePinch_Title_Element) Pinch_EnablePinch_Title = Pinch_EnablePinch_Title_Element.get("value");

        var Pinch_DeleteData_Title_Element = pinchPluginNode.find('variable[@name="PINCH_DELETEDATA_TITLE"]');
        if (Pinch_DeleteData_Title_Element) Pinch_DeleteData_Title = Pinch_DeleteData_Title_Element.get("value");

        var Pinch_Setttings_Title_Element = pinchPluginNode.find('variable[@name="PINCH_SETTINGS_TITLE"]');
        if (Pinch_Setttings_Title_Element) Pinch_Setttings_Title = Pinch_Setttings_Title_Element.get("value");

        var Pinch_Authorization_PrimaryColor_Element = pinchPluginNode.find('variable[@name="PINCH_AUTHORIZATION_PRIMARYCOLOR"]');
        if (Pinch_Authorization_PrimaryColor_Element) Pinch_Authorization_PrimaryColor = Pinch_Authorization_PrimaryColor_Element.get("value");

        var Pinch_Authorization_SecondaryColor_Element = pinchPluginNode.find('variable[@name="PINCH_AUTHORIZATION_SECONDARYCOLOR"]');
        if (Pinch_Authorization_SecondaryColor_Element) Pinch_Authorization_SecondaryColor = Pinch_Authorization_SecondaryColor_Element.get("value");

        var Pinch_Authorization_LocationImage_Element = pinchPluginNode.find('variable[@name="PINCH_AUTHORIZATION_LOCATIONIMAGE"]');
        if (Pinch_Authorization_LocationImage_Element) Pinch_Authorization_LocationImage = Pinch_Authorization_LocationImage_Element.get("value");

        var Pinch_Authorization_NotificationImage_Element = pinchPluginNode.find('variable[@name="PINCH_AUTHORIZATION_NOTIFICATIONIMAGE"]');
        if (Pinch_Authorization_NotificationImage_Element) Pinch_Authorization_NotificationImage = Pinch_Authorization_NotificationImage_Element.get("value");

        var Pinch_AuthorizeOnStartup_Element = pinchPluginNode.find('variable[@name="PINCH_AUTHORIZEONSTARTUP"]');
        if (Pinch_AuthorizeOnStartup_Element) Pinch_AuthorizeOnStartup = Pinch_AuthorizeOnStartup_Element.get("value");

        var Pinch_UseAuthorizationUI_Element = pinchPluginNode.find('variable[@name="PINCH_USEAUTHORIZATIONUI"]');
        if (Pinch_UseAuthorizationUI_Element) Pinch_UseAuthorizationUI = Pinch_UseAuthorizationUI_Element.get("value");

        var Pinch_HandleAuthorization_Element = pinchPluginNode.find('variable[@name="PINCH_HANDLEAUTHORIZATION"]');
        if (Pinch_HandleAuthorization_Element) Pinch_HandleAuthorization = Pinch_HandleAuthorization_Element.get("value");
        

        if (context.opts.cordova.platforms.indexOf("ios") > -1) {


            var resourcesDir = path.join(context.opts.projectRoot, 'platforms/ios/' + APPNAME + '/');
            var plist = new ConfigFile(resourcesDir, 'ios', APPNAME + '-Info.plist');
            var plistChanged = false;
            if (!plist.data.hasOwnProperty("NSLocationWhenInUseUsageDescription")) {
                plist.data["NSLocationWhenInUseUsageDescription"] = App_LocationWhenInUseDescription;
                console.log("PINCH: iOS - Adding text for NSLocationWhenInUseUsageDescription in " + APPNAME + "-Info.plist");
                plistChanged = true;
            }
            if (!plist.data.hasOwnProperty("NSLocationAlwaysUsageDescription")) {
                plist.data["NSLocationAlwaysUsageDescription"] = App_LocationAlwaysDescription;
                console.log("PINCH: iOS - Adding text for NSLocationAlwaysUsageDescription in " + APPNAME + "-Info.plist");
                plistChanged = true;
            }
            if (!plist.data.hasOwnProperty("NSLocationAlwaysAndWhenInUseUsageDescription")) {
                plist.data["NSLocationAlwaysAndWhenInUseUsageDescription"] = App_LocationAlwaysDescription;
                console.log("PINCH: iOS - Adding text for NSLocationAlwaysAndWhenInUseUsageDescription in " + APPNAME + "-Info.plist");
                plistChanged = true;
            }
            if (!plist.data.hasOwnProperty("CFBundleURLTypes") && App_UrlScheme!="" && App_UrlName!="") {
                var urlScheme = JSON.parse('[{"CFBundleURLSchemes":["' + App_UrlScheme + '"],"CFBundleURLName":"' + App_UrlName + '"}]');
                plist.data["CFBundleURLTypes"] = urlScheme;
                console.log("PINCH: iOS - Adding entries for URLSchemes in " + APPNAME + "-Info.plist");
                plistChanged = true;
            }
            if (plistChanged) {
                plist.save();
                console.log("PINCH: iOS - Saved changes to Info.plist");
            }


            var pinchBundleDir = path.join(context.opts.projectRoot, 'platforms/ios/' + APPNAME + '/Resources/Pinch.bundle');
            var pinchPlist = new ConfigFile(pinchBundleDir, 'ios', 'Pinch.plist');
            pinchPlist.data.AppID = Pinch_AppId;
            pinchPlist.data.AppSecret = Pinch_AppSecret;

            pinchPlist.data.AuthorizeOnStartup = Pinch_AuthorizeOnStartup=="true";
            pinchPlist.data.UseAuthorizationUI = Pinch_UseAuthorizationUI=="true";
            pinchPlist.data.HandleAuthorization = Pinch_HandleAuthorization=="true";

            if (Pinch_BubbleBackgroundColor != "") pinchPlist.data.EventBubbleBackgroundColor = Pinch_BubbleBackgroundColor;
            if (Pinch_BubbleBorderColor != "") pinchPlist.data.EventBubbleBorderColor = Pinch_BubbleBorderColor;
            if (Pinch_BubbleForegroundColor != "") pinchPlist.data.EventBubbleForegroundColor = Pinch_BubbleForegroundColor;
            if (Pinch_Authorization_PrimaryColor != "") {
                pinchPlist.data.DialogTitleColor = Pinch_Authorization_PrimaryColor;
                pinchPlist.data.DialogButtonColor = Pinch_Authorization_PrimaryColor;
            }
            if (Pinch_Authorization_SecondaryColor != "") pinchPlist.data.DialogRadioButtonColor = Pinch_Authorization_SecondaryColor;
            if (Pinch_Authorization_LocationImage != "") {
              pinchPlist.data.DialogLocationImageName = Pinch_Authorization_LocationImage;
              pinchPlist.data.DialogLocationAlwaysImageName = Pinch_Authorization_LocationImage;
            }
            if (Pinch_Authorization_NotificationImage != "") pinchPlist.data.DialogNotificationImageName = Pinch_Authorization_NotificationImage;

            pinchPlist.save();
            console.log("PINCH: iOS - Updated Pinch.bundle/Pinch.plist from preferences");
            console.log(pinchPlist.filepath);

            var settingsBundleDir = path.join(context.opts.projectRoot, 'platforms/ios/' + APPNAME + '/Resources/Settings.bundle');
            var settingsRootPlist = new ConfigFile(settingsBundleDir, 'ios', 'Root.plist');
            for(key in settingsRootPlist.data.PreferenceSpecifiers) {
              var entry = settingsRootPlist.data.PreferenceSpecifiers[key];
              if (Pinch_Setttings_Title != "" && entry.Type=="PSGroupSpecifier") entry.Title = Pinch_Setttings_Title;
              if (Pinch_EnablePinch_Title != "" && entry.hasOwnProperty("Key") && entry.Key == "EnablePinch") entry.Title = Pinch_EnablePinch_Title;
              if (Pinch_DeleteData_Title != "" && entry.hasOwnProperty("Key") && entry.Key == "DeletePinchData") entry.Title = Pinch_DeleteData_Title;
            }
            settingsRootPlist.save();
            console.log("PINCH: iOS - Updated Settings.bundle from preferences:");
            console.log(settingsRootPlist.filepath);
        }        

        if (context.opts.cordova.platforms.indexOf("android") > -1) {

            
            console.log("PINCH: Android - Updating configuration files from preferences");                   

            var androidPrjDir = path.join(context.opts.projectRoot, 'platforms/android/app/src/main');
            var androidManifest = new ConfigFile(androidPrjDir, 'android', 'AndroidManifest.xml');

            var resDir = path.join(androidPrjDir, 'res/values');
            var pinchXml = new ConfigFile(resDir, 'android', 'pinch_settings.xml');
            var drawableDir = path.join(androidPrjDir, 'res/drawable');
            var roundButtonXml = new ConfigFile(drawableDir, 'android', 'pinch_round_button.xml');
            if (Pinch_BubbleBackgroundColor != "") roundButtonXml.data.find("solid").set("android:color", Pinch_BubbleBackgroundColor);
            if (Pinch_BubbleBorderColor != "") roundButtonXml.data.find("stroke").set("android:color", Pinch_BubbleBorderColor);
		    roundButtonXml.save();

            console.log("PINCH: Android - Updated bubble color preference in round_button.xml");

            pinchXml.data.find('string[@name="AppID"]').text = Pinch_AppId;
            pinchXml.data.find('string[@name="AppSecret"]').text = Pinch_AppSecret;
            pinchXml.data.find('string[@name="UrlScheme"]').text = App_UrlScheme + "://" + App_UrlName;
            pinchXml.data.find('string[@name="NotificationTitle"]').text = APPNAME;
            pinchXml.data.find('string[@name="HandleAuthorization"]').text = Pinch_AuthorizeOnStartup;
            pinchXml.save();

            console.log("PINCH: Android - Updated app settings in pinch.xml");


            var manifestApplicationName = DefaultApplicationName;
            var isMultiDexEnabled = false;
            var multiDexEnabled = configXml.data.find('.//preference[@name="android-multiDexEnabled"]')
            if (multiDexEnabled && multiDexEnabled.get("value") == "true") {                                
                var manifestApplicationName = MultiDexApplicationName;
                var isMultiDexEnabled = true;
                console.log("PINCH: Android - MultiDexEnabled Detected");
            }           
            
            var applicationNode = androidManifest.data.find('application');
            if (applicationNode.get('android:name') === undefined) {
                applicationNode.set('android:name', manifestApplicationName);
                console.log("PINCH: Android - Setting Manifest Application's android:name: '" + manifestApplicationName + "'");
            } else {                                
                if (!isMultiDexEnabled) {
                    var message = "PINCH: Android - It looks like your Manifest Application's android:name has already been set. Please add this to onCreate in your custom application class:  PinchManager.getInstance().initAndStartAsync(this);";
                    console.log('\x1b[33m\x1b[1m%s\x1b[0m: ', message);
                } else {
                    var message = "PINCH: Android - MultiDex - Replacing Manifest Application android:name: '" + manifestApplicationName + "'";                    
                    applicationNode.set('android:name', manifestApplicationName);                  
                    console.log(message);  
                }
            }

            var intentDataNode = androidManifest.data.find('.//intent-filter[@android:name="pinch"]/data');
            intentDataNode.set("android:scheme", App_UrlScheme);
            intentDataNode.set("android:host", App_UrlName);
            androidManifest.save();
            console.log("PINCH: Android - Added intent filter with url scheme and host in AndroidManifest.xml");


        }

        console.log("PINCH: Configuration completed successfully.");

        return true;
    } catch (ex) {

        console.log('\x1b[33m\x1b[1m%s\x1b[0m', ex);
        console.log('\x1b[31m\x1b[1m%s\x1b[0m', "PINCH: Failed to install 'cordova-plugin-pinch' with proper settings. Uninstalling.");
        exec("cordova plugin rm cordova-plugin-pinch");

        //throw(ex);
    }

    function replaceGradleReference() {

        //echo "OVERWRITING GENERATED build.gradle IN PROJECT, AS LONG AS WE DO    NOT FIND A BETTER WAY"
        //cp resources/build.gradle platforms/android/build.gradle

        var gradleLocation = process.cwd() + "/plugins/cordova-plugin-pinch/src/android/pinch.gradle";
        console.log(gradleLocation);
        fs.readFile(gradleLocation, 'utf8', function (err, data) {
            console.log(data);
            console.log(err);
        });
      } 
}
