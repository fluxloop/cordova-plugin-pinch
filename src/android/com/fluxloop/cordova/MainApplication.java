package com.fluxloop.cordova;

import android.app.Application;

import com.pinch.library.manager.PinchManager;

/**
 * Created by jarle on 06.02.2017.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        PinchManager.getInstance().initAndStartAsync(this);
    }

    @Override
    public void onTerminate() {

        PinchManager.getInstance().clear();
        super.onTerminate();
    }
}
