package com.fluxloop.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pinch.library.api.dto.beacon.Campaign;
import com.pinch.library.listener.PinchEventListener;
import com.pinch.library.manager.PinchManager;
import com.pinch.library.util.Timber;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebSettings;
/**
 * This class echoes a string called from JavaScript.
 */
public class Pinch extends CordovaPlugin {

    private static CordovaWebView mwebView;
    private static CordovaInterface mcordova;

    public static final String TAG = "Pinch";


    private void FireEvent(String name, String data)
    {
        String globalJs = String.format("javascript:cordova.fireDocumentEvent('pinch', ['%s','%s']);", name, data);
        sendJS(globalJs);

        String specificJs = String.format("javascript:cordova.fireDocumentEvent('%s', ['%s']);", name, data);
        sendJS(specificJs);
    }

    @Override
    public void initialize(CordovaInterface cordova, final CordovaWebView webView) {

        super.initialize(cordova, webView);

        mwebView = super.webView;
        mcordova = cordova;

        PinchManager.getInstance().setListener(new PinchEventListener() {
            @Override
            public void onEvent(String name, String data) {
                // ...
                // PinchEventOnCampaignReceived
                // PinchEventOnCampaignClosed
                // PinchEventOnCampaignDeleted
                // PinchEventOnCampaignExpired
                // PinchEventOnCampaignOpened

                FireEvent(name, data);

            }
        });

 	    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            ((WebView) webView.getView()).getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);


        PinchManager.getInstance().askPermissionsIfNotAsked(false, false);
    }

    private void sendJS(final String js) {
        mcordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mwebView.loadUrl(js);
            }
        });
    }

    @Override
    public void onResume(boolean multitasking) {

        PinchManager.getInstance().onResume();
        super.onResume(multitasking);
    }

    @Override
    public void onPause(boolean multitasking) {
        PinchManager.getInstance().onPause();

        super.onPause(multitasking);
    }

    @Override
    public boolean execute(String action, JSONArray args, org.apache.cordova.CallbackContext callbackContext) throws JSONException {

        currentContext = callbackContext;

        try {
            if (action.equals("pinchRegisterUserData")) {
                String modelName = args.getString(0);
                String userData = args.getString(1);
                this.pinchRegisterUserData(modelName, userData, callbackContext);
                return true;
            } else if (action.equals("pinchGetPinchID")) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().spManager.getPinchId()));
                return true;
            } else if (action.equals("pinchDeleteCampaign")) {
                PinchManager.getInstance().pinchDeleteCampaign();
                return true;
            }
            else if(action.equals("pinchGetLatestCampaign"))
            {
                JSONObject returnObject = null;
                Campaign campaign = null;
                try {
                    campaign = PinchManager.getInstance().pinchGetCurrentCampaign();
                    if(campaign!=null) {
                        String campaignJson = new Gson().toJson(campaign);

                        returnObject = new JSONObject(campaignJson);
                    }
                }
                catch (Exception ex)
                {
                    Log.e(TAG, ex.getMessage());
                }

                if(returnObject!=null)
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, returnObject));
                else
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));

                return true;
            }
            else if(action.equals("pinchSimulateCampaign"))
            {
                Campaign campaign = new Campaign();
                campaign.setBubbleImage("pinch_icon_info.png");
                campaign.setCampaignWebUrl("https://ganymede.blob.core.windows.net/campaign-ticket/SparDemo1/index.html?id=2");
                campaign.setMessageExpireSeconds(3600);
                campaign.setShowEventBubble(true);
                campaign.setShowLocalNotification(true);
                campaign.setShowCampOnAppForeground(true);
                campaign.setShowCampOnReceiveNotification(true);
                campaign.setMessageId(java.util.UUID.randomUUID().toString());

                campaign.setViewType(1);

                campaign.setNotificationText("Test campaign");

                PinchManager.getInstance().pinchSimulateCampaign(campaign);

                return true;
            }

            else if(action.equals("pinchCloseCampaign"))
            {
                PinchManager.getInstance().pinchCloseCampaign();
                return true;

            }

            else if(action.equals("pinchDeleteCampaign"))
            {
                PinchManager.getInstance().pinchDeleteCampaign();
                return true;
            }
            else if(action.equals("pinchOpenCampaign"))
            {
                PinchManager.getInstance().pinchOpenCampaign();
                return true;
            }
            else if(action.equals("pinchOpenCampaignWithUrl"))
            {
                String url = args.getString(0);
                Integer viewtype = args.getInt(1);

                Campaign campaign = new Campaign();
                campaign.setBubbleImage("pinch_icon_info.png");
                campaign.setCampaignWebUrl(url);
                campaign.setMessageExpireSeconds(3600);
                campaign.setShowEventBubble(true);
                campaign.setShowLocalNotification(true);
                campaign.setShowCampOnAppForeground(true);
                campaign.setShowCampOnReceiveNotification(true);
                campaign.setMessageId(java.util.UUID.randomUUID().toString());

                campaign.setViewType(viewtype);

                campaign.setNotificationText("Test campaign");

                PinchManager.getInstance().pinchSimulateCampaign(campaign);
                return true;
            }
            else if(action.equals("getAdvertisingIdentifier"))
            {
                //return success
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getAdvertisingId()));
                return true;
            }

            else if(action.equals("pinchRegisterUserName"))
            {
                String userName = args.getString(0);
                PinchManager.getInstance().pinchRegisterUserName(userName);
                return true;
            }

            else if(action.equals("pinchRegisterEvent"))
            {
                String eventName = args.getString(0);
                String eventData = null;

                if(args.length()>1)
                    eventData = args.getString(1);

                PinchManager.getInstance().pinchRegisterEvent(eventName,eventData);
                return true;
            }
            else if(action.equals("pinchSetPinchEnabled"))
            {
                Boolean enabled = args.getBoolean(0);
                PinchManager.getInstance().setEnabled(enabled);
                return true;
            }
            else if(action.equals("pinchGetPinchEnabled"))
            {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getEnabled()));
                return true;
            }
            else if(action.equals("pinchCheckAuthorization"))
            {
                Boolean ignoreAlreadyAsked = false;
                
                if(args!=null && args.length()>1)
                    ignoreAlreadyAsked = args.getBoolean(0);
                    
                PinchManager.getInstance().askPermissionsIfNotAsked(ignoreAlreadyAsked, true);
            }
            else if(action.equals("pinchCheckBluetoothStatus"))
            {
                //success
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getIsBluetoothEnabled()));
                return true;
            }
            else if(action.equals("pinchCheckNotificationSettings"))
            {
                //success
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().hasNotificationPermission()));
                return true;
            }

            else if(action.equals("pinchRequestNotificationAuthorization"))
            {
                PinchManager.getInstance().spManager.setNotificationPermissionAsked(true);

                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().hasNotificationPermission()));

                return true;

            }

            else if(action.equals("pinchRequestLocationAuthorization") || action.equals("pinchRequestLocationAlwaysAuthorization")  || action.equals("pinchRequestLocationWhenInUseAuthorization"))
            {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                {
                    PinchManager.getInstance().spManager.setPermissionAsked(true);
                }

                JSONArray permissions = new JSONArray();
                int requestId = storeContextByRequestId();
                permissions.put("ACCESS_COARSE_LOCATION");
                permissions.put("ACCESS_FINE_LOCATION");

                _requestRuntimePermissions(permissions, requestId);

                return true;
            }

            else if(action.equals("pinchHasNotificationPermission"))
            {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().hasNotificationPermission()));

                return true;
            }

            else if(action.equals("pinchHasLocationPermission"))
            {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getHasLocationPermission()));

                return true;
            }

            else if(action.equals("pinchGetLocationRequested"))
            {

                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getHasAskedLocationPermission()));

                return true;
            }

            else if(action.equals("pinchGetNotificationRequested"))
            {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getHasAskedNotificationPermission()));

                return true;
            }

            else if(action.equals("pinchGetUserDefaults"))
            {
                //key
                String key = args.getString(0);
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PinchManager.getInstance().getUserDefaultString(key)));

                return true;
            }

            else if(action.equals("pinchSaveUserDefaults"))
            {
                //key,value
                String key = args.getString(0);
                String value = args.getString(1);

                PinchManager.getInstance().saveUserDefaultString(key, value);

                return true;
            }

            else if(action.equals("pinchRemoveUserDefaults"))
            {
                //key
                String key = args.getString(0);
                PinchManager.getInstance().removeUserDefaultString(key);

                return true;
            }
        }
        catch (Exception ex)
        {
            callbackContext.error(ex.getMessage());
        }
        return false;

    }


    public static LocationManager locationManager;

    /**
     * Current Cordova callback context (on this thread)
     */
    protected CallbackContext currentContext;

    private static String gpsLocationPermission = "ACCESS_FINE_LOCATION";
    private static String networkLocationPermission = "ACCESS_COARSE_LOCATION";
    private static String externalStoragePermission = "READ_EXTERNAL_STORAGE";

    /**
     * Either user denied permission and checked "never ask again"
     * Or authorisation has not yet been requested for permission
     */
    private static final String STATUS_NOT_REQUESTED_OR_DENIED_ALWAYS = "STATUS_NOT_REQUESTED_OR_DENIED_ALWAYS";

    private HashMap<String, CallbackContext> callbackContexts = new HashMap<String, CallbackContext>();

    private HashMap<String, JSONObject> permissionStatuses = new HashMap<String, JSONObject>();

    private static final Map<String, String> permissionsMap;
    static {
        Map<String, String> _permissionsMap = new HashMap <String, String>();
        Pinch.addBiDirMapEntry(_permissionsMap, "READ_CALENDAR", Manifest.permission.READ_CALENDAR);
        Pinch.addBiDirMapEntry(_permissionsMap, "WRITE_CALENDAR", Manifest.permission.WRITE_CALENDAR);
        Pinch.addBiDirMapEntry(_permissionsMap, "CAMERA", Manifest.permission.CAMERA);
        Pinch.addBiDirMapEntry(_permissionsMap, "READ_CONTACTS", Manifest.permission.READ_CONTACTS);
        Pinch.addBiDirMapEntry(_permissionsMap, "WRITE_CONTACTS", Manifest.permission.WRITE_CONTACTS);
        Pinch.addBiDirMapEntry(_permissionsMap, "GET_ACCOUNTS", Manifest.permission.GET_ACCOUNTS);
        Pinch.addBiDirMapEntry(_permissionsMap, "ACCESS_FINE_LOCATION", Manifest.permission.ACCESS_FINE_LOCATION);
        Pinch.addBiDirMapEntry(_permissionsMap, "ACCESS_COARSE_LOCATION", Manifest.permission.ACCESS_COARSE_LOCATION);
        Pinch.addBiDirMapEntry(_permissionsMap, "RECORD_AUDIO", Manifest.permission.RECORD_AUDIO);
        Pinch.addBiDirMapEntry(_permissionsMap, "READ_PHONE_STATE", Manifest.permission.READ_PHONE_STATE);
        Pinch.addBiDirMapEntry(_permissionsMap, "CALL_PHONE", Manifest.permission.CALL_PHONE);
        Pinch.addBiDirMapEntry(_permissionsMap, "ADD_VOICEMAIL", Manifest.permission.ADD_VOICEMAIL);
        Pinch.addBiDirMapEntry(_permissionsMap, "USE_SIP", Manifest.permission.USE_SIP);
        Pinch.addBiDirMapEntry(_permissionsMap, "PROCESS_OUTGOING_CALLS", Manifest.permission.PROCESS_OUTGOING_CALLS);
        Pinch.addBiDirMapEntry(_permissionsMap, "SEND_SMS", Manifest.permission.SEND_SMS);
        Pinch.addBiDirMapEntry(_permissionsMap, "RECEIVE_SMS", Manifest.permission.RECEIVE_SMS);
        Pinch.addBiDirMapEntry(_permissionsMap, "READ_SMS", Manifest.permission.READ_SMS);
        Pinch.addBiDirMapEntry(_permissionsMap, "RECEIVE_WAP_PUSH", Manifest.permission.RECEIVE_WAP_PUSH);
        Pinch.addBiDirMapEntry(_permissionsMap, "RECEIVE_MMS", Manifest.permission.RECEIVE_MMS);
        Pinch.addBiDirMapEntry(_permissionsMap, "WRITE_EXTERNAL_STORAGE", Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Pinch.addBiDirMapEntry(_permissionsMap, "READ_CALL_LOG", Manifest.permission.READ_CALL_LOG);
        Pinch.addBiDirMapEntry(_permissionsMap, "WRITE_CALL_LOG", Manifest.permission.WRITE_CALL_LOG);
        Pinch.addBiDirMapEntry(_permissionsMap, "READ_EXTERNAL_STORAGE", Manifest.permission.READ_EXTERNAL_STORAGE);
        Pinch.addBiDirMapEntry(_permissionsMap, "BODY_SENSORS", Manifest.permission.BODY_SENSORS);
        permissionsMap = Collections.unmodifiableMap(_permissionsMap);
    }

    private static void addBiDirMapEntry(Map map, Object key, Object value){
        map.put(key, value);
        map.put(value, key);
    }

    /**
     * User authorised permission
     */
    private static final String STATUS_GRANTED = "GRANTED";

    /**
     * User denied permission (without checking "never ask again")
     */
    private static final String STATUS_DENIED = "DENIED";

    public void requestRuntimePermissions(JSONArray args) throws Exception{
        JSONArray permissions = args.getJSONArray(0);
        int requestId = storeContextByRequestId();
        _requestRuntimePermissions(permissions, requestId);
    }

    public void requestRuntimePermission(JSONArray args) throws Exception {
        requestRuntimePermission(args.getString(0));
    }

    public void requestRuntimePermission(String permission) throws Exception{
        requestRuntimePermission(permission, storeContextByRequestId());
    }

    public void requestRuntimePermission(String permission, int requestId) throws Exception{
        JSONArray permissions = new JSONArray();
        permissions.put(permission);
        _requestRuntimePermissions(permissions, requestId);
    }

    private void _requestRuntimePermissions(JSONArray permissions, int requestId) throws Exception{
        JSONObject currentPermissionsStatuses = _getPermissionsAuthorizationStatus(jsonArrayToStringArray(permissions));
        JSONArray permissionsToRequest = new JSONArray();



        for(int i = 0; i<currentPermissionsStatuses.names().length(); i++){
            String permission = currentPermissionsStatuses.names().getString(i);
            boolean granted = currentPermissionsStatuses.getString(permission) == Pinch.STATUS_GRANTED;
            if(granted){
                Log.d(TAG, "Permission already granted for "+permission);
                JSONObject requestStatuses = permissionStatuses.get(String.valueOf(requestId));
                requestStatuses.put(permission, Pinch.STATUS_GRANTED);
                permissionStatuses.put(String.valueOf(requestId), requestStatuses);
            }else{
                String androidPermission = permissionsMap.get(permission);
                Log.d(TAG, "Requesting permission for "+androidPermission);
                permissionsToRequest.put(androidPermission);
            }
        }
        if(permissionsToRequest.length() > 0){
            Log.v(TAG, "Requesting permissions");
            requestPermissions(this, requestId, jsonArrayToStringArray(permissionsToRequest));

        }else{
            Log.d(TAG, "No permissions to request: returning result");

            sendRuntimeRequestResult(requestId);
        }
    }

    private void requestPermissions(CordovaPlugin plugin, int requestCode, String [] permissions) throws Exception{
        try {
            java.lang.reflect.Method method = cordova.getClass().getMethod("requestPermissions", org.apache.cordova.CordovaPlugin.class ,int.class, java.lang.String[].class);
            method.invoke(cordova, plugin, requestCode, permissions);
        } catch (NoSuchMethodException e) {
            throw new Exception("requestPermissions() method not found in CordovaInterface implementation of Cordova v" + CordovaWebView.CORDOVA_VERSION);
        }
    }

    private JSONObject _getPermissionsAuthorizationStatus(String[] permissions) throws Exception{
        JSONObject statuses = new JSONObject();
        for(int i=0; i<permissions.length; i++){
            String permission = permissions[i];
            if (!permissionsMap.containsKey(permission)){
                throw new Exception("Permission name '"+permission+"' is not a valid permission");
            }
            String androidPermission = permissionsMap.get(permission);
            Log.v(TAG, "Get authorisation status for "+androidPermission);
            boolean granted = hasPermission(androidPermission);
            if (granted){
                statuses.put(permission, Pinch.STATUS_GRANTED);
            }else{
                boolean showRationale = shouldShowRequestPermissionRationale(this.cordova.getActivity(), androidPermission);
                if(!showRationale){
                    statuses.put(permission, Pinch.STATUS_NOT_REQUESTED_OR_DENIED_ALWAYS);
                }else{
                    statuses.put(permission, Pinch.STATUS_DENIED);
                }
            }
        }
        return statuses;
    }
    private boolean shouldShowRequestPermissionRationale(Activity activity, String permission) throws Exception{
        boolean shouldShow;
        try {
            java.lang.reflect.Method method = ActivityCompat.class.getMethod("shouldShowRequestPermissionRationale", Activity.class, java.lang.String.class);
            Boolean bool = (Boolean) method.invoke(null, activity, permission);
            shouldShow = bool.booleanValue();
        } catch (NoSuchMethodException e) {
            throw new Exception("shouldShowRequestPermissionRationale() method not found in ActivityCompat class. Check you have Android Support Library v23+ installed");
        }
        return shouldShow;
    }

    private void locationStatusCallback(Boolean hasLocationAuthorization, CallbackContext context)
    {
        JsonObject payload = new JsonObject();

        payload.addProperty("PinchID", PinchManager.getInstance().spManager.getPinchId());

        if(hasLocationAuthorization)
        {
            payload.addProperty("LocationServicesEnabled", true);
            payload.addProperty("AuthorizationStatus", 3);
            payload.addProperty("Status", "Always");

            FireEvent("pinchLocationAuthorizationStatus", new Gson().toJson(payload));

            context.success("Always");
        }
        else
        {
            payload.addProperty("LocationServicesEnabled", false);
            payload.addProperty("AuthorizationStatus", 0);
            payload.addProperty("Status", "Never");


            FireEvent("pinchLocationAuthorizationStatus", new Gson().toJson(payload));

            context.success("Never");
        }
    }

    private void sendRuntimeRequestResult(int requestId){
        String sRequestId = String.valueOf(requestId);
        CallbackContext context = callbackContexts.get(sRequestId);
        JSONObject statuses = permissionStatuses.get(sRequestId);
        Log.v(TAG, "Sending runtime request result for id=" + sRequestId);

        if(statuses!=null && statuses.has("ACCESS_COARSE_LOCATION"))
        {
            Boolean hasLocationAuthorization = false;

            try {

                hasLocationAuthorization = statuses.getString("ACCESS_COARSE_LOCATION")=="GRANTED";
            }
          catch (Exception ex)
          {
              Log.e(TAG, ex.getMessage());
          }


            locationStatusCallback(hasLocationAuthorization, context);

        }
        else {

            context.success(statuses);
        }

    }

    private int storeContextByRequestId(){
        String requestId = generateRandomRequestId();
        callbackContexts.put(requestId, currentContext);
        permissionStatuses.put(requestId, new JSONObject());
        return Integer.valueOf(requestId);
    }

    private boolean hasPermission(String permission) throws Exception{
        boolean hasPermission = true;
        Method method = null;
        try {
            method = cordova.getClass().getMethod("hasPermission", permission.getClass());
            Boolean bool = (Boolean) method.invoke(cordova, permission);
            hasPermission = bool.booleanValue();
        } catch (NoSuchMethodException e) {
            Log.w(TAG, "Cordova v" + CordovaWebView.CORDOVA_VERSION + " does not support runtime permissions so defaulting to GRANTED for " + permission);
        }
        return hasPermission;
    }


    private String generateRandomRequestId(){
        String requestId = null;

        while(requestId == null){
            requestId = generateRandom();
            if(callbackContexts.containsKey(requestId)){
                requestId = null;
            }
        }
        return requestId;
    }

    private String generateRandom(){
        Random rn = new Random();
        int random = rn.nextInt(1000000) + 1;
        return Integer.toString(random);
    }

    private String[] jsonArrayToStringArray(JSONArray array) throws JSONException{
        if(array==null)
            return null;

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }

    private CallbackContext getContextById(String requestId) throws Exception{
        if (!callbackContexts.containsKey(requestId)) {
            throw new Exception("No context found for request id=" + requestId);
        }
        return callbackContexts.get(requestId);
    }

    private void clearRequest(int requestId){
        String sRequestId = String.valueOf(requestId);
        if (!callbackContexts.containsKey(sRequestId)) {
            return;
        }
        callbackContexts.remove(sRequestId);
        permissionStatuses.remove(sRequestId);
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
        String sRequestId = String.valueOf(requestCode);
        Log.v(TAG, "Received result for permissions request id=" + sRequestId);
        try {

            CallbackContext context = getContextById(sRequestId);
            JSONObject statuses = permissionStatuses.get(sRequestId);

            for (int i = 0, len = permissions.length; i < len; i++) {
                String androidPermission = permissions[i];
                String permission = permissionsMap.get(androidPermission);
                String status;
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = shouldShowRequestPermissionRationale(this.cordova.getActivity(), androidPermission);
                    if (!showRationale) {
                        // EITHER: The app doesn't have a permission and the user has not been asked for the permission before
                        // OR: user denied WITH "never ask again"
                        status = Pinch.STATUS_NOT_REQUESTED_OR_DENIED_ALWAYS;
                    } else {
                        // user denied WITHOUT "never ask again"
                        status = Pinch.STATUS_DENIED;
                    }
                } else {
                    // Permission granted
                    status = Pinch.STATUS_GRANTED;
                }
                statuses.put(permission, status);
                Log.v(TAG, "Authorisation for " + permission + " is " + statuses.get(permission));
                clearRequest(requestCode);
            }

            if(statuses!=null && statuses.has("ACCESS_COARSE_LOCATION"))
            {
                PinchManager.getInstance().spManager.setPermissionAsked(true);

                Boolean hasLocationAuthorization = false;

                try {

                    hasLocationAuthorization = statuses.getString("ACCESS_COARSE_LOCATION")=="GRANTED";
                }
                catch (Exception ex)
                {
                    Log.e(TAG, ex.getMessage());
                }

                locationStatusCallback(hasLocationAuthorization, context);
            }
            else {

                context.success(statuses);
            }

        }catch(Exception e ) {
            handleError("Exception occurred onRequestPermissionsResult: ".concat(e.getMessage()), requestCode);
        }
    }

    private void handleError(String errorMsg, CallbackContext context){
        try {
            Log.e(TAG, errorMsg);
            context.error(errorMsg);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /**
     * Handles an error while executing a plugin API method in the current context.
     * Calls the registered Javascript plugin error handler callback.
     * @param errorMsg Error message to pass to the JS error handler
     */
    private void handleError(String errorMsg) {
        handleError(errorMsg, currentContext);
    }

    /**
     * Handles error during a runtime permissions request.
     * Calls the registered Javascript plugin error handler callback
     * then removes entries associated with the request ID.
     * @param errorMsg Error message to pass to the JS error handler
     * @param requestId The ID of the runtime request
     */
    private void handleError(String errorMsg, int requestId){
        CallbackContext context;
        String sRequestId = String.valueOf(requestId);
        if (callbackContexts.containsKey(sRequestId)) {
            context = callbackContexts.get(sRequestId);
        }else{
            context = currentContext;
        }
        handleError(errorMsg, context);
        clearRequest(requestId);
    }



    private void pinchRegisterUserData(String modelName, String userData, org.apache.cordova.CallbackContext callbackContext) {
        PinchManager.getInstance().pinchRegisterUserData(userData,modelName);

        callbackContext.success();
    }


}