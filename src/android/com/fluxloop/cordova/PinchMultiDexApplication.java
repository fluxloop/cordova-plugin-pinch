package com.fluxloop.cordova;
import android.app.Application;
import com.pinch.library.manager.PinchManager;

public class PinchMultiDexApplication extends android.support.multidex.MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        PinchManager.getInstance().initAndStartAsync(this);
    }

    @Override
    public void onTerminate() {

        PinchManager.getInstance().clear();
        super.onTerminate();
    }
}
