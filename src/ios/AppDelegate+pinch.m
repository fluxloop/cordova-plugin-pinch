#import "AppDelegate+pinch.h"
#import "CDVPinch.h"
#import <objc/runtime.h>


@implementation AppDelegate (notification)


// its dangerous to override a method from within a category.
// Instead we will use method swizzling. we set this up in the load call.
+ (void)load
{
    Method original, swizzled;
    original = class_getInstanceMethod(self, @selector(init));
    swizzled = class_getInstanceMethod(self, @selector(swizzled_init));
    method_exchangeImplementations(original, swizzled);
}

- (AppDelegate *)swizzled_init
{
    // Init pinch early
    [[PinchLibrary sharedInstance] pinchEnableJavascriptEvents:true];
    [[PinchLibrary sharedInstance] start];

    // Register observer for finishLaunching
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishLaunching:) name:@"UIApplicationDidFinishLaunchingNotification" object:nil];

    // Calls the original init method over in AppDelegate.
    return [self swizzled_init];
}

// This code will be called immediately after application:didFinishLaunchingWithOptions. We need to process notifications in cold-start situations
- (void)finishLaunching:(NSNotification *)notification
{
    [[PinchLibrary sharedInstance] pinchApplicationDidFinishLaunching:notification];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[PinchLibrary sharedInstance] pinchDidReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[PinchLibrary sharedInstance] pinchDidFailToRegisterForRemoteNotificationsWithError:error];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[PinchLibrary sharedInstance] pinchDidRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
    NSString *eventName = @"pinchNotificationAuthorizationStatus";
    NSDictionary *eventData = @{@"Enabled": [NSNumber numberWithBool:self.hasNotificationPermission]};
    NSDictionary *innerPayload = @{@"eventName": eventName, @"eventData": eventData};

    // Post observable notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pinchEvent" object:self userInfo:innerPayload];
    [[NSNotificationCenter defaultCenter] postNotificationName:eventName object:self userInfo:eventData];
}

-(BOOL)hasNotificationPermission {
    // iOS 8 Notifications
    if ([UIApplication instancesRespondToSelector:@selector(currentUserNotificationSettings)]) {
        UIUserNotificationType types = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
        if (types & UIUserNotificationTypeAlert) return TRUE;
        return FALSE;
    } else {
        // Notification is always allowed for pre iOS 8
        return TRUE;
    }
}



@end
