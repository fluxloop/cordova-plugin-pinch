//
//  PinchLibrary+CDVPinch.h
//  Pinch
//
//  Created by fluxloop on 23/11/15.
//
//

#import <PinchLibrary/PinchLibrary.h>
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <Cordova/CDV.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface CDVPinch : CDVPlugin <CBCentralManagerDelegate>

@property (nonatomic, strong) CBCentralManager* bluetoothManager;
@property (nonatomic, strong) NSString* requestLocationAlwaysCallbackId;
@property (nonatomic, strong) NSString* requestLocationCallbackId;
@property (nonatomic, strong) NSString* requestLocationWhenInUseCallbackId;
@property (nonatomic, strong) NSString* requestNotificationCallbackId;
@property (nonatomic, strong) UIButton* closeButton;
@property (nonatomic, strong) NSString* defaultWebUrl;

- (void)pinchInitViews:(CDVInvokedUrlCommand*)command;
- (void)pinchOpenCampaign:(CDVInvokedUrlCommand*)command;
- (void)pinchOpenCampaignWithUrl:(CDVInvokedUrlCommand*)command;
- (void)pinchSimulateCampaign:(CDVInvokedUrlCommand*)command;
- (void)pinchSimulateCampaignWithData:(CDVInvokedUrlCommand*)command;
- (void)pinchShowDebugButtons:(CDVInvokedUrlCommand*)command;
- (void)pinchDeleteCampaign:(CDVInvokedUrlCommand*)command;
- (void)pinchCloseCampaign:(CDVInvokedUrlCommand*)command;
- (void)pinchSetPinchEnabled:(CDVInvokedUrlCommand*)command;
- (void)pinchRegisterUserName:(CDVInvokedUrlCommand*)command;
- (void)pinchRegisterUserData:(CDVInvokedUrlCommand*)command;
- (void)pinchRegisterEvent:(CDVInvokedUrlCommand*)command;
- (void)pinchGetAdvertisingIdentifier:(CDVInvokedUrlCommand*)command;
- (void)pinchSetAdvertisingIdentifier:(CDVInvokedUrlCommand*)command;
- (void)pinchGetLatestCampaign:(CDVInvokedUrlCommand*)command;
- (void)pinchCheckAuthorization:(CDVInvokedUrlCommand*)command;
- (void)pinchCheckNotificationSettings:(CDVInvokedUrlCommand*)command;
- (void)pinchCheckBluetoothStatus:(CDVInvokedUrlCommand*)command;

- (void)pinchRequestNotificationAuthorization:(CDVInvokedUrlCommand*)command;
- (void)pinchRequestLocationAuthorization:(CDVInvokedUrlCommand*)command;
- (void)pinchRequestLocationWhenInUseAuthorization:(CDVInvokedUrlCommand*)command;
- (void)pinchRequestLocationAlwaysAuthorization:(CDVInvokedUrlCommand*)command;
- (void)pinchHasNotificationPermission:(CDVInvokedUrlCommand*)command;
- (void)pinchHasLocationPermission:(CDVInvokedUrlCommand*)command;
- (void)pinchGetLocationRequested:(CDVInvokedUrlCommand*)command;
- (void)pinchGetNotificationRequested:(CDVInvokedUrlCommand*)command;
- (void)pinchGetUserDefaults:(CDVInvokedUrlCommand*)command;
- (void)pinchSaveUserDefaults:(CDVInvokedUrlCommand*)command;
- (void)pinchRemoveUserDefaults:(CDVInvokedUrlCommand*)command;

- (void)pinchPinchShowCloseButton:(CDVInvokedUrlCommand*)command;
- (void)pinchPinchHideCloseButton:(CDVInvokedUrlCommand*)command;

- (void)pinchGetLocationSettings:(CDVInvokedUrlCommand*)command;
- (void)pinchGetNotificationSettings:(CDVInvokedUrlCommand*)command;
- (void)pinchOpenAppSettings:(CDVInvokedUrlCommand*)command;
- (void)pinchSetIdleTimerDisabled:(CDVInvokedUrlCommand*)command;

- (void)loadUrl:(CDVInvokedUrlCommand*)command;


@end
