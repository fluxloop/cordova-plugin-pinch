//
//  PinchLibrary.h
//  SelfService
//
//  Created by fluxloop on 13/10/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface PinchLibrary : NSObject

typedef enum CampaignViewType { CampaignNoShow, CampaignShowPopover, CampaignShowFullscreen, CampaignShowExternal } CampaignViewType;
typedef enum CampaignStatus { CampaignReceived, CampaignOpened, CampaignClosed, CampaignApplicationLaunched, CampaignDeleted, CampaignExpired, CampaignAccepted, CampaignGoalAchieved } CampaignStatus;

FOUNDATION_EXPORT NSString *const PinchEventOnCampaignReceived;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaignDeleted;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaignClosed;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaignExpired;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaignOpened;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaignReady;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaignGoalAchieved;
FOUNDATION_EXPORT NSString *const PinchEventOnCampaigWebviewLoaded;
FOUNDATION_EXPORT NSString *const PinchEventOnGlobalEvent;



- (void) start;
- (void) start:(UIView*)rootView;
- (void) start:(NSString*)appId appSecret:(NSString*)appSecret;
- (void) start:(UIView*)rootView appId:(NSString*)appId appSecret:(NSString*)appSecret;
- (void) updateRootView:(UIView*)rootView;
- (void) pinchSetAdvertisingIdentifier:(NSUUID*)idfa;
- (void) pinchRegisterUserData:(NSDictionary*)userData userDataModelName:(NSString*)userDataModelName;
- (void) pinchRegisterUserName:(NSString*)userName;
- (void) pinchRegisterEvent:(NSString*)eventName;
- (void) pinchRegisterEventWithData:(NSString*)eventName eventData:(NSDictionary*)eventData;
- (void) pinchSavePrivateUserData:(NSDictionary*)userData;
- (void) pinchSavePrivateUserName:(NSString*)userName;
- (void) pinchCheckAuthorization;
- (void) pinchDidReceiveLocalNotification:(UILocalNotification *)notification;
- (void) pinchApplicationDidFinishLaunching:(NSNotification*)notification;
- (void) pinchInitViews:(UIView*)rootView;

- (void) pinchShowDebugButtons;
- (void) pinchEnableJavascriptEvents:(BOOL)enable;

- (void) simulateReceivedCampaignWithData:(NSDictionary*) beaconData;

- (void) pinchDidReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler: (void (^)(UIBackgroundFetchResult)) completionHandler;

- (void) pinchDidRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void) pinchDidFailToRegisterForRemoteNotificationsWithError:(NSError *)error;

- (void) pinchOpenCampaign;
- (void) pinchOpenCampaignWithUrl:(NSString*) urlString viewType:(int)viewTypeInt;
- (void) pinchOpenCampaignWithUrlWaitLoaded:(NSString*) urlString viewType:(int)viewTypeInt waitSeconds:(int)waitSeconds;

- (NSDictionary*) pinchGetCurrentCampaign;
- (void) pinchCloseCampaign;
- (void) pinchDeleteCampaign;
- (NSString *) pinchGetPinchID;

- (void) pinchSetPinchEnabled:(BOOL)enabled;
- (BOOL) pinchGetPinchEnabled;
- (BOOL) loadingWebview;
- (BOOL) waitLoadingWebview;


- (void) pinchRequestNotificationAuthorization;
- (void) pinchRequestLocationAuthorization;
- (void) pinchRequestLocationAlwaysAuthorization;
- (void) pinchRequestLocationWhenInUseAuthorization;
- (BOOL) pinchHasNotificationPermission;
- (BOOL) pinchHasLocationPermission;
- (BOOL) pinchGetLocationRequested;
- (BOOL) pinchGetNotificationRequested;

- (void) pinchSaveUserDefaults:(NSString*)key value:(NSString*)value;
- (NSString *) pinchGetUserDefaults:(NSString*)key;
- (void) pinchRemoveUserDefaults:(NSString*)key;


+ (instancetype)sharedInstance;



@end
