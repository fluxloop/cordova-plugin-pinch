//
//  PinchLibrary+CDVPinch.m
//  Pinch
//
//  Created by fluxloop on 23/11/15.
//
//

#import "CDVPinch.h"
#import <Cordova/CDVViewController.h>
#import <Cordova/CDVScreenOrientationDelegate.h>
#import <AdSupport/ASIdentifierManager.h>
#import <CoreLocation/CoreLocation.h>


@implementation CDVPinch



- (void)pluginInitialize
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageDidLoad) name:CDVPageDidLoadNotification object:self.webView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidResume:) name: UIApplicationDidBecomeActiveNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pinchEventHandler:) name:@"pinchEvent" object:nil];

    self.bluetoothManager = [[CBCentralManager alloc]
                             initWithDelegate: self
                             queue: dispatch_get_main_queue()
                             options: @{CBCentralManagerOptionShowPowerAlertKey: @(NO)}];
}


- (void)pinchEventHandler:(NSNotification *)notification {

    // Get notification data
    NSDictionary *dataObject = [notification userInfo];
    NSString *eventName = dataObject[@"eventName"];
    NSString *eventData = @"";

    // Check if event has data
    if (dataObject[@"eventData"] != [NSNull null]) {
        NSData *eventDataObject = [NSJSONSerialization dataWithJSONObject:dataObject[@"eventData"] options:0 error:nil];
        eventData= [[NSString alloc] initWithData:eventDataObject encoding:NSUTF8StringEncoding];
    }

    // Fire global pinch event
    [self.commandDelegate evalJs:[NSString stringWithFormat:@"cordova.fireDocumentEvent('pinch', ['%@','%@']); ", eventName, eventData]];

    // Fire specific pinch event
    [self.commandDelegate evalJs:[NSString stringWithFormat:@"cordova.fireDocumentEvent('%@', ['%@','%@']); ", eventName, eventName, eventData]];

}

- (void) didRegisterUserNotificationSettings:(UIUserNotificationSettings*)settings
{
    NSLog(@"didRegisterUserNotificationSettings");
}

- (void)finishLaunching:(NSNotification *)notification
{
    NSLog(@"didRegisterUserNotificationSettings");

    // DEFINE CLOSE BUTTON
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.backgroundColor = [UIColor blackColor];
    self.closeButton.frame = CGRectMake(self.webView.bounds.size.width-44-20,20,44, 44);
    self.closeButton.alpha = 0.7;
    self.closeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.closeButton.layer.cornerRadius = self.closeButton.frame.size.width/2;
    [self.closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.closeButton setTitle:@"X" forState:UIControlStateNormal];
    [self.closeButton setHidden:TRUE];
    [self.closeButton addTarget:self action:@selector(resetWebview) forControlEvents:UIControlEventTouchUpInside];
    [self.webView  addSubview:self.closeButton];

}

- (void)pinchPinchShowCloseButton:(CDVInvokedUrlCommand*)command
{
    if (self.closeButton!=nil) {
        [self.closeButton setHidden:FALSE];
    }
}

- (void)pinchPinchHideCloseButton:(CDVInvokedUrlCommand*)command
{
    if (self.closeButton!=nil) {
        [self.closeButton setHidden:TRUE];
    }
}

- (void)resetWebview {
    if(self.defaultWebUrl!=nil){
        NSURL *url = [NSURL URLWithString:self.defaultWebUrl];
        [self.webViewEngine loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (void)applicationDidResume:(UIApplication *)application
{
    NSLog(@"applicationDidResume");

    [[PinchLibrary sharedInstance] pinchCheckAuthorization];

    if (self.requestLocationAlwaysCallbackId!=nil || self.requestLocationWhenInUseCallbackId!=nil || self.requestNotificationCallbackId!=nil || self.requestLocationCallbackId!=nil) {
        [self.commandDelegate runInBackground:^{

            if (self.requestNotificationCallbackId!=nil) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.1), dispatch_get_main_queue(), ^(void){
                    NSString *messageString = [self notificationPermission]?@"true":@"false";
                    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.requestNotificationCallbackId];
                    self.requestNotificationCallbackId = nil;
                });
            }

            if (self.requestLocationAlwaysCallbackId!=nil || self.requestLocationWhenInUseCallbackId || self.requestLocationCallbackId) {

                NSString* authorizationStatus = @"NotDetermined";
                if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) authorizationStatus = @"Denied";
                if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted) authorizationStatus = @"Restricted";
                if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse) authorizationStatus = @"WhenInUse";
                if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways) authorizationStatus = @"Always";
                CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:authorizationStatus];

                if (self.requestLocationWhenInUseCallbackId!=nil) {
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.requestLocationWhenInUseCallbackId];
                    self.requestLocationWhenInUseCallbackId = nil;
                }
                if (self.requestLocationAlwaysCallbackId!=nil) {
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.requestLocationAlwaysCallbackId];
                    self.requestLocationAlwaysCallbackId = nil;
                }
                if (self.requestLocationCallbackId!=nil) {
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.requestLocationCallbackId];
                    self.requestLocationCallbackId = nil;
                }
            }

        }];
    }
}

- (void)pageDidLoad
{
    // Check if application has kept any beacon data and parse it
    NSDictionary *beaconData = [self loadDictionary:@"BeaconData"];

    NSString* jsString = @"document.location.href";
    [self.webViewEngine evaluateJavaScript:jsString
                         completionHandler:^(id object, NSError* error) {
                             if ((error == nil) && [object isKindOfClass:[NSString class]]) {
                                 NSString* url = (NSString*)object;

                                 if (self.defaultWebUrl!=nil) {
                                     NSString *baseUrl =  [self.defaultWebUrl substringWithRange:NSMakeRange(0, 20)];
                                     if ([url hasPrefix:baseUrl] || [baseUrl hasPrefix:@"file://"]) {
                                         [self.closeButton setHidden:TRUE];
                                     } else {
                                         [self.closeButton setHidden:FALSE];
                                     }
                                 } else {
                                     self.defaultWebUrl =  url;
                                     [self.closeButton setHidden:TRUE];
                                 }
                             }
                         }];


    if ([beaconData count]) {
        NSDictionary *payload = @{@"EventName": @"pinchCampaignAccepted", @"EventData":beaconData};

        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:nil];
        NSString *serializedPayload= [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        NSString *javascriptString = [NSString stringWithFormat:@"pinchRegisterEvent('%@');", serializedPayload];
        [(UIWebView*)self.webView stringByEvaluatingJavaScriptFromString:javascriptString];

        // Fire general and specific events
        [self.commandDelegate evalJs:[NSString stringWithFormat:@"cordova.fireDocumentEvent('pinch', ['pinchCampaignAccepted','%@']); ", serializedPayload]];
        [self.commandDelegate evalJs:[NSString stringWithFormat:@"cordova.fireDocumentEvent('pinchCampaignAccepted', ['%@']); ", serializedPayload]];

    }

}


-(void)loadUrl:(CDVInvokedUrlCommand*)command {
    NSString* urlString = [command.arguments objectAtIndex:0];

    if (urlString) {
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [(UIWebView*)self.webView loadRequest:urlRequest];
    }
}

- (void)pinchGetLatestCampaign:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult = nil;
    NSDictionary *beaconData = [self loadDictionary:@"BeaconData"];
    if ([beaconData count]) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:beaconData];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)pinchGetAdvertisingIdentifier:(CDVInvokedUrlCommand*)command
{

    // Check command.arguments here.
    [self.commandDelegate runInBackground:^{

        if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled])
        {
            NSUUID *IDFA = [[ASIdentifierManager sharedManager] advertisingIdentifier];
            NSString *IDFA_String = [IDFA UUIDString];

            // Some blocking logic...
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:IDFA_String];
            // The sendPluginResult method is thread-safe.
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

        } else {
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }

    }];

}

- (void)pinchGetPinchID:(CDVInvokedUrlCommand*)command {

    [self.commandDelegate runInBackground:^{

        // Get pinchID from Pinch
        NSString *pinchID = [[PinchLibrary sharedInstance] pinchGetPinchID];

        // Send results thread safe
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:pinchID];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

- (void)pinchCheckAuthorization:(CDVInvokedUrlCommand*)command
{
    [[PinchLibrary sharedInstance] pinchCheckAuthorization];
}

- (void)pinchSetAdvertisingIdentifier:(CDVInvokedUrlCommand*)command
{
}

// First argument should be username
- (void)pinchRegisterUserName:(CDVInvokedUrlCommand*)command
{
         NSString* userName = [command.arguments objectAtIndex:0];
        if (userName!=nil) {
            [[PinchLibrary sharedInstance] pinchRegisterUserName:userName];
        }

}

- (void)pinchOpenCampaign:(CDVInvokedUrlCommand*)command
{
    [[PinchLibrary sharedInstance] pinchOpenCampaign];
}

- (void)pinchOpenCampaignWithUrl:(CDVInvokedUrlCommand*)command
{
    NSString* url = [command.arguments objectAtIndex:0];
    int viewtypeInt = 1;

    // Check if viewtype is defined
    if ([command.arguments count]>1) {
        NSString* viewtype = [command.arguments objectAtIndex:1];
        viewtypeInt = [viewtype intValue];
    }

    // Check if both parameters is defined with correct type and not null
    if (url && viewtypeInt>0) {

        // Send userdata dictionary and modelname
        [[PinchLibrary sharedInstance] pinchOpenCampaignWithUrl:url viewType:viewtypeInt];
    }
}


- (void)pinchShowDebugButtons:(CDVInvokedUrlCommand*)command
{
    [[PinchLibrary sharedInstance] pinchShowDebugButtons];
}

- (void)pinchDeleteCampaign:(CDVInvokedUrlCommand*)command
{
    [[PinchLibrary sharedInstance] pinchDeleteCampaign];
}

- (void)pinchCloseCampaign:(CDVInvokedUrlCommand*)command
{
    [[PinchLibrary sharedInstance] pinchCloseCampaign];
}

- (void)pinchSetPinchEnabled:(CDVInvokedUrlCommand*)command
{
    id value = [command argumentAtIndex:0];
    if (!([value isKindOfClass:[NSNumber class]])) {
        value = [NSNumber numberWithBool:YES];
    }

    [[PinchLibrary sharedInstance] pinchSetPinchEnabled:[value boolValue]];
}


// First argument should be serialized userdata
- (void)pinchRegisterUserData:(CDVInvokedUrlCommand*)command
{
    NSString* modelname = [command.arguments objectAtIndex:0];
    NSString* userdata = [command.arguments objectAtIndex:1];

    // Check if both parameters is defined with correct type and not null
    if (userdata && modelname) {

        // Parse json string to dicationary
        NSError *jsonError;
        NSData *objectData = [userdata dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *userDataDictionary = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];

        // Send userdata dictionary and modelname
        [[PinchLibrary sharedInstance] pinchRegisterUserData:userDataDictionary userDataModelName:modelname];
    }
}

- (void)pinchRegisterEvent:(CDVInvokedUrlCommand*)command
{
    NSString* eventName = [command argumentAtIndex:0];
    if (eventName) {
        [[PinchLibrary sharedInstance] pinchRegisterEvent:eventName];
    }
}


// CHECK IF BLUETOOTH IS ENABLED
- (void)pinchCheckBluetoothStatus:(CDVInvokedUrlCommand*)command {

    NSString* status = [self bluetoothStatus];

    [self.commandDelegate runInBackground:^{

        // Some blocking logic...
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: status];
        // The sendPluginResult method is thread-safe.
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

-(NSString*) bluetoothStatus {
    if(!self.bluetoothManager) {
        self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }
    return [NSString stringWithFormat:@"%ld", (long)self.bluetoothManager.state];
}


- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"Bluetooth was enabled");
        [[self commandDelegate] evalJs:[NSString stringWithFormat:@"cordova.fireWindowEvent('BluetoothStatus.enabled');"]];
    } else if([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"Bluetooth was disabled");
        [[self commandDelegate] evalJs:[NSString stringWithFormat:@"cordova.fireWindowEvent('BluetoothStatus.disabled');"]];
    }
}

//  CHECK IF NOTIFICATION IS ENABLED
-(void)pinchCheckNotificationSettings:(CDVInvokedUrlCommand*)command{

    [self.commandDelegate runInBackground:^{

        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        NSMutableString* statusString = [NSMutableString string];
        if (grantedSettings.types & UIUserNotificationTypeNone)  [statusString appendString:@"None"];
        if (grantedSettings.types & UIUserNotificationTypeAlert) [statusString appendString:@"Alert"];
        if (grantedSettings.types & UIUserNotificationTypeSound) [statusString appendString:@"Sound"];
        if (grantedSettings.types & UIUserNotificationTypeBadge) [statusString appendString:@"Badge"];

        // Some blocking logic...
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:statusString];
        // The sendPluginResult method is thread-safe.
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}


- (void)pinchSimulateCampaignWithData:(CDVInvokedUrlCommand*)command
{
    NSString* campaignData = [command.arguments objectAtIndex:0];
    if (campaignData) {

        // Parse json string to dicationary
        NSError *jsonError;
        NSData *objectData = [campaignData dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *campaignDataDictionary = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];

        // Send campaign object to pinch
        [[PinchLibrary sharedInstance] simulateReceivedCampaignWithData:campaignDataDictionary];
    }
}


- (void)pinchSimulateCampaign:(CDVInvokedUrlCommand*)command
{
    // Random MessageID for testing
    int randomInt = (int)1 + arc4random() % (999);
    NSString* randomNumber = [NSString stringWithFormat:@"%d",randomInt];

    NSDictionary *campaignData =  @{@"PinchID": @"0000000000000000", @"MessageID":randomNumber, @"MessageExpireSeconds":[NSNumber numberWithInt:320],@"CampaignViewType":[NSNumber numberWithInt:CampaignShowPopover], @"CampaignWebUrl":@"http://gulltaggen.azurewebsites.net/debug.html", @"ShowCampaignOnApplicationForeground":[NSNumber numberWithBool:FALSE],@"ShowCampaignOnReceivedNotification":[NSNumber numberWithBool:FALSE], @"ShowCampaignOnApplicationLaunch":[NSNumber numberWithBool:FALSE], @"NotificationText":@"En liten melding", @"ShowEventBubble":[NSNumber numberWithBool:TRUE], @"EventBubbleImageName":@"pinch_icon_info.png", @"ShowLocalNotification":[NSNumber numberWithBool:TRUE]};
    ;

    [[PinchLibrary sharedInstance] simulateReceivedCampaignWithData:campaignData];
}


- (void)pinchInitViews:(CDVInvokedUrlCommand*)command
{
    [[PinchLibrary sharedInstance] pinchInitViews:self.viewController.view];
}

- (void)pinchRequestNotificationAuthorization:(CDVInvokedUrlCommand*)command
{
    if ([[PinchLibrary sharedInstance] pinchGetNotificationRequested]) {
        NSString *messageString = [[PinchLibrary sharedInstance] pinchHasNotificationPermission]?@"true":@"false";
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        self.requestNotificationCallbackId = nil;
    }
    else {
        self.requestNotificationCallbackId = command.callbackId;
        [[PinchLibrary sharedInstance] pinchRequestNotificationAuthorization];
    }
}


- (NSString*) authorizationStatus {
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined) return @"NotDetermined";
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) return @"Denied";
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted) return @"Restricted";
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse) return @"WhenInUse";
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways) return @"Always";
    return @"NotDetermined";
}

-(BOOL)notificationPermission {
    UIUserNotificationType types = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
    if (types & UIUserNotificationTypeAlert) return TRUE;
    return FALSE;
}


- (void)pinchRequestLocationAuthorization:(CDVInvokedUrlCommand*)command
{
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse ) {
        self.requestLocationCallbackId = command.callbackId;
        [[PinchLibrary sharedInstance] pinchRequestLocationAuthorization];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self authorizationStatus]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        self.requestLocationCallbackId = nil;
    }
}

- (void)pinchRequestLocationAlwaysAuthorization:(CDVInvokedUrlCommand*)command
{
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse ) {
        self.requestLocationAlwaysCallbackId = command.callbackId;
        [[PinchLibrary sharedInstance] pinchRequestLocationAlwaysAuthorization];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self authorizationStatus]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        self.requestLocationAlwaysCallbackId = nil;
    }
}

- (void)pinchRequestLocationWhenInUseAuthorization:(CDVInvokedUrlCommand*)command
{
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined) {
        self.requestLocationWhenInUseCallbackId = command.callbackId;
        [[PinchLibrary sharedInstance] pinchRequestLocationWhenInUseAuthorization];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self authorizationStatus]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        self.requestLocationWhenInUseCallbackId = nil;
    }
}

- (void)pinchHasNotificationPermission:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get result from Pinch
        NSString *messageString = [[PinchLibrary sharedInstance] pinchHasNotificationPermission]?@"true":@"false";

        // Send results thread safe
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

- (void)pinchHasLocationPermission:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get result from Pinch
        NSString *messageString = [[PinchLibrary sharedInstance] pinchHasLocationPermission]?@"true":@"false";

        // Send results thread safe
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

- (void)pinchGetLocationRequested:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get result from Pinch
        NSString *messageString = [[PinchLibrary sharedInstance] pinchGetLocationRequested]?@"true":@"false";

        // Send results thread safe
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

- (void)pinchGetNotificationRequested:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get result from Pinch
        NSString *messageString = [[PinchLibrary sharedInstance] pinchGetNotificationRequested]?@"true":@"false";

        // Send results thread safe
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

- (void)pinchGetUserDefaults:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get key for user defaults
        NSString* key = [command.arguments objectAtIndex:0];

        // If key is defined
        if (key.length) {

            // Get result from Pinch
            NSString *messageString = [[PinchLibrary sharedInstance] pinchGetUserDefaults:key];

            // Send results thread safe
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:messageString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

        }

    }];
}

- (void)pinchSaveUserDefaults:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get key for user defaults
        NSString* key = [command.arguments objectAtIndex:0];
        NSString* value = [command.arguments objectAtIndex:1];

        // If key and value is defined
        if (key.length && value.length) {

            // Save key and value in user defaults via pinch
            [[PinchLibrary sharedInstance] pinchSaveUserDefaults:key value:value];
        }

    }];
}

- (void)pinchRemoveUserDefaults:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        // Get key for user defaults
        NSString* key = [command.arguments objectAtIndex:0];

        // If key is defined
        if (key.length) {

            // Remove user default
            [[PinchLibrary sharedInstance] pinchRemoveUserDefaults:key];

        }

    }];
}

- (void)pinchGetLocationSettings:(CDVInvokedUrlCommand*)command {
    // Send results thread safe
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self authorizationStatus]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)pinchGetNotificationSettings:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{

        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        NSMutableString* statusString = [NSMutableString string];
        if (grantedSettings.types & UIUserNotificationTypeNone)  [statusString appendString:@"None"];
        if (grantedSettings.types & UIUserNotificationTypeAlert) [statusString appendString:@"Alert"];
        if (grantedSettings.types & UIUserNotificationTypeSound) [statusString appendString:@"Sound"];
        if (grantedSettings.types & UIUserNotificationTypeBadge) [statusString appendString:@"Badge"];

        // Some blocking logic...
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:statusString];
        // The sendPluginResult method is thread-safe.
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}

- (void)pinchOpenAppSettings:(CDVInvokedUrlCommand*)command {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)pinchSetIdleTimerDisabled:(CDVInvokedUrlCommand*)command {
    bool enable = [[command.arguments objectAtIndex:0] boolValue];
    [UIApplication sharedApplication].idleTimerDisabled = enable;
}

- (NSDictionary*) loadDictionary:(NSString*) key {
    return (NSDictionary *)[[NSUserDefaults standardUserDefaults] objectForKey:key];
}

@end
