var exec = require('cordova/exec');

var pinch = {
    initViews:function() {
        exec(null, null, "Pinch", "pinchInitViews", []);
    },
    requestNotificationAuthorization:function(callback) {
        exec(callback, null, "Pinch", "pinchRequestNotificationAuthorization", []);
    },
    requestLocationAuthorization:function(callback) {
        exec(callback, null, "Pinch", "pinchRequestLocationAuthorization", []);
    },
    requestLocationAlwaysAuthorization:function(callback) {
        exec(callback, null, "Pinch", "pinchRequestLocationAlwaysAuthorization", []);
    },
    requestLocationWhenInUseAuthorization:function(callback) {
        exec(callback, null, "Pinch", "pinchRequestLocationWhenInUseAuthorization", []);
    },
    hasNotificationPermission:function(success) {
        exec(success, null, "Pinch", "pinchHasNotificationPermission", []);
    },
    hasLocationPermission:function(success) {
        exec(success, null, "Pinch", "pinchHasLocationPermission", []);
    },
    getLocationRequested:function(success) {
        exec(success, null, "Pinch", "pinchGetLocationRequested", []);
    },
    getNotificationRequested:function(success) {
        exec(success, null, "Pinch", "pinchGetNotificationRequested", []);
    },
    getUserDefaults:function(key, success) {
        exec(success, null, "Pinch", "pinchGetUserDefaults", [key]);
    },
    saveUserDefaults:function(key, value, success) {
        exec(success, null, "Pinch", "pinchSaveUserDefaults", [key,value]);
    },
    removeUserDefaults:function(key) {
        exec(null, null, "Pinch", "pinchRemoveUserDefaults", [key]);
    },
    simulateCampaign:function() {
        exec(null, null, "Pinch", "pinchSimulateCampaign", []);
    },
    closeCampaign:function() {
        exec(null, null, "Pinch", "pinchCloseCampaign", []);
    },
    deleteCampaign:function() {
        exec(null, null, "Pinch", "pinchDeleteCampaign", []);
    },
    openCampaign:function() {
        exec(null, null, "Pinch", "pinchOpenCampaign", []);
    },
    openCampaignWithUrl:function(url,viewtype) {
        exec(null, null, "Pinch", "pinchOpenCampaignWithUrl", [url,viewtype]);
    },
    getPinchID:function(success) {
        exec(success, null, "Pinch", "pinchGetPinchID", []);
    },
    getAdvertisingIdentifier:function(success) {
        exec(success, null, "Pinch", "pinchGetAdvertisingIdentifier", []);
    },
    getLatestCampaign:function(success) {
        exec(success, null, "Pinch", "pinchGetLatestCampaign", []);
    },
    registerUserName:function(userName) {
        exec(null, null, "Pinch", "pinchRegisterUserName", [userName]);
    },
    registerUserData:function(modelname,userdata) {
        exec(null, null, "Pinch", "pinchRegisterUserData", [modelname,userdata]);
    },
    registerEvent:function(eventName) {
        exec(null, null, "Pinch", "pinchRegisterEvent", [eventName]);
    },
    showDebugButtons:function(enabled) {
        exec(null, null, "Pinch", "pinchSetPinchEnabled", [enabled]);
    },
    setPinchEnabled:function(enabled) {
        exec(null, null, "Pinch", "pinchSetPinchEnabled", [enabled]);
    },
    checkAuthorization:function() {
        exec(null, null, "Pinch", "pinchCheckAuthorization", []);
    },
    checkBluetoothStatus:function(success) {
        exec(success, null, "Pinch", "pinchCheckBluetoothStatus", []);
    },
    checkNotificationSettings:function(success) {
        exec(success, null, "Pinch", "pinchCheckNotificationSettings", []);
    },
    checkLocationSettings:function(success) {
        exec(success, null, "Pinch", "pinchCheckLocationSettings", []);
    },
    getLocationSettings:function(success) {
        exec(success, null, "Pinch", "pinchGetLocationSettings", []);
    },
    getNotificationSettings:function(success) {
        exec(success, null, "Pinch", "pinchGetNotificationSettings", []);
    },
    openAppSettings:function() {
        exec(null, null, "Pinch", "pinchOpenAppSettings", []);
    },
    setIdleTimerDisabled:function(enabled) {
        exec(null, null, "Pinch", "pinchSetIdleTimerDisabled", [enabled]);
    },
    showCloseButton:function(show) {
        if (show) exec(null, null, "Pinch", "pinchPinchShowCloseButton", []);
        else exec(null, null, "Pinch", "pinchPinchHideCloseButton", []);
    },
    addEventListener: function (eventSelector, callback) {
        document.addEventListener(eventSelector, function (data) {
            var eventName = data[0];var eventData = null;
            if (data[1]) eventData = JSON.parse(data[1]);
            callback(eventName, eventData);
        }, true);
    }

};

module.exports = pinch;
